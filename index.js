/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printWelcomeMessage(){
	let fullName = prompt("Enter your fullname:");
	let age = prompt("Enter your age:");
	let location = prompt("Enter your location:");
	alert("Thank you!");

	console.log(fullName);
	console.log(age);
	console.log(location);
}
printWelcomeMessage();



	

/*	
	

	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function musicalArtist(){
	let band1 ="1. Callalily";
	let band2 ="2. Parokya ni Edgar";
	let band3 ="3. Siakol";
	let band4 ="4. Rivermaya";
	let band5 ="5. Eraserheads";

	console.log(band1);
	console.log(band2);
	console.log(band3);
	console.log(band4);
	console.log(band5);
}

musicalArtist();




/*	
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function favoriteMovies(){
	let movie1 ="1. V for Vendetta";
	let rating1 ="Rotten Tomatoes Rating: 73%";

	let movie2 ="2. Iron man";
	let rating2 ="Rotten Tomatoes Rating: 94%";

	let movie3 ="3. Harry Potter";
	let rating3 ="Rotten Tomatoes Rating: 81%";

	let movie4 ="4. The Dark Knight";
	let rating4 ="Rotten Tomatoes Rating: 94%";

	let movie5 ="5. Joker";
	let rating5 ="Rotten Tomatoes Rating: 69%";

	console.log(movie1);
	console.log(rating1);

	console.log(movie2);
	console.log(rating2);

	console.log(movie3);
	console.log(rating3);

	console.log(movie4);
	console.log(rating4);

	console.log(movie5);
	console.log(rating5);
}

favoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3);
}


// console.log(friend1);
// console.log(friend2);

printFriends();



